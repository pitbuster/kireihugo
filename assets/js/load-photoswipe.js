/*
Put this file in /static/js/load-photoswipe.js
Documentation and licence at https://github.com/liwenyip/hugo-easy-gallery/
*/

function testAVIF() {
  return new Promise((res) => {
    const avif = new Image();
    avif.src =
      "data:image/avif;base64,AAAAHGZ0eXBhdmlmAAAAAGF2aWZtaWYxbWlhZgAAAPBtZXRhAAAAAAAAAChoZGxyAAAAAAAAAABwaWN0AAAAAAAAAAAAAAAAbGliYXZpZgAAAAAOcGl0bQAAAAAAAQAAAB5pbG9jAAAAAEQAAAEAAQAAAAEAAAEUAAAAHAAAAChpaW5mAAAAAAABAAAAGmluZmUCAAAAAAEAAGF2MDFDb2xvcgAAAABoaXBycAAAAElpcGNvAAAAFGlzcGUAAAAAAAAAAQAAAAIAAAAOcGl4aQAAAAABCAAAAAxhdjFDgQ0cAAAAABNjb2xybmNseAACAAIAAYAAAAAXaXBtYQAAAAAAAAABAAEEAQKDBAAAACRtZGF0EgAKCBgAHsgQEAwgMg5EgAAAEAAAEBVcDAbgcA==";
    avif.onload = avif.onerror = function () {
      res(avif.height === 2);
    };
  });
}

function testWebP() {
  return new Promise((res) => {
    const webP = new Image();
    webP.src =
      "data:image/webp;base64,UklGRioAAABXRUJQVlA4IB4AAACQAQCdASoBAAIAD8D+JaQAAuUt8gAA/U97bhgAAAA=";
    webP.onload = webP.onerror = function () {
      res(webP.height === 2);
    };
  });
}

document.addEventListener("DOMContentLoaded", async function () {
  const supportAVIF = await testAVIF();
  const supportWebP = await testWebP();
  // array of slide objects that will be passed to PhotoSwipe()
  let items = [];
  // for every figure element on the page:
  document.querySelectorAll("figure").forEach(function ($figure, index) {
    if ($figure.className == "no-photoswipe")
      return true; // ignore any figures where class="no-photoswipe"
    const $a = $figure.querySelector("a");
    const $img = $figure.querySelector("img");
    const $div = $figure.querySelector("div");
    const $caption = $figure.querySelector("figcaption");
    let $src = $a.href;
    let $title = "";
    if ($caption !== null)
      $title = $caption.querySelector("p").innerHTML;
    const $msrc = $img.src;
    let $avifUsed = false;
    if (supportAVIF) {
      $newSrc = $src.replace(".jpg", ".avif");
      $newSrc = $newSrc.replace(".png", ".avif");
      if ($div.classList.contains("avif"))
      {
         $src = $newSrc;
         $avifUsed = true;
      }
    }
    if (!$avifUsed && supportWebP) {
      $newSrc = $src.replace(".jpg", ".webp");
      $newSrc = $newSrc.replace(".png", ".webp");
      if ($div.classList.contains("webp"))
         $src = $newSrc;
    }

    let item = {
      src: $src,
      w: 400, // temp default size
      h: 300, // temp default size
      title: $title,
      msrc: $msrc,
    };

    // if data-size on figure is set, read it and create an item
    let $dataSize = $div.attributes["data-size"];
    if ($dataSize) {
      const $size = $dataSize.value.split("x");
      item.w = $size[0];
      item.h = $size[1];
      //console.log("Using pre-defined dimensions for " + $src);
    // if not, set temp default size then load the image to check actual size
    } else {
      //console.log("Using default dimensions for " + $src);
      // load the image to check its dimensions
      // update the item as soon as w and h are known (check every 30ms)
      let img = new Image();
      img.src = $src;
      let wait = setInterval(function () {
        const w = img.naturalWidth;
        const h = img.naturalHeight;
        if (w && h) {
          clearInterval(wait);
          item.w = w;
          item.h = h;
          //console.log("Got actual dimensions for " + img.src);

          // Update the figure parent div's max width.
          const $parentDiv = $figure.parentElement;
          if (!$parentDiv.hasOwnProperty("style")) {
            $figure.parentElement.style = "max-width: " + w + "px";
          }
        }
      }, 30);
    }
    // Save the index of this image then add it to the array
    items.push(item);
    // Event handler for click on a figure
    $figure.addEventListener("click", function (event) {
      event.preventDefault(); // prevent the normal behaviour i.e. load the <a> hyperlink
      // Get the PSWP element and initialize it with the desired options
      const $pswp = document.querySelector(".pswp");
      let options = {
        index: index,
        bgOpacity: 0.8,
        showHideOpacity: true,
      };
      new PhotoSwipe($pswp, PhotoSwipeUI_Default, items, options).init();
    }, false);
  });
}, false);
